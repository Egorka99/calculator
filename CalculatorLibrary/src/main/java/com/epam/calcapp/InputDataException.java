package com.epam.calcapp;


/**
 * Exception class for invalid input
 */
public class InputDataException extends Exception {

    public InputDataException(String message) {
        super(message);
    }
}
