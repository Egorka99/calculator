package com.epam.calcapp;

/**
 * Exception class for incorrect calculation
 */
public class CalculationException extends Exception{
    public CalculationException(String message) {
        super(message);
    }
}
