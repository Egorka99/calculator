package com.epam.calcapp;

import java.util.List;
import java.util.Stack;

/**
 * Evaluate expression
 * @author Egor_Zavgorodnev
 */
public class ExpressionEvaluation implements Calculator {

    /**
     * Stack of operations in expression
     */
    private Stack<Operation> operationStack;
 
    /**
     * Stack of arguments in expression
     */
    private Stack<Double> argumentStack;


    @Override
    public double calculate(String expression) throws CalculationException, InputDataException {

       List<Part> parts = new StringWithExpressionHandler(expression).breakStringIntoParts();

        operationStack = new Stack<>();
        argumentStack = new Stack<>();

        for (Part currentPart : parts) {
            switch (currentPart.type) {
                case sign:
                    Operation currentOperation = new Operation();
                    currentOperation.sign = currentPart.value.charAt(0);
                    currentOperation.priority = 0;
                    switch (currentOperation.sign) {
                        case '+':
                        case '-':
                            currentOperation.priority = 1;
                            break;
                        case '*':
                        case '/':
                            currentOperation.priority = 2;
                            break;
                    }
                    calculateCurrentState(currentOperation.priority);
                    operationStack.push(currentOperation);
                    break;
                case number:
                    argumentStack.push(Double.parseDouble(currentPart.value));
                    break;
            }
        }

            calculateCurrentState(0);

        return argumentStack.lastElement();
    }


    /**
     * Recursive method to calculate the value of an expression
     * @param minPriority
     * @throws InputDataException
     */
    private void calculateCurrentState(int minPriority) throws CalculationException {
        if (operationStack.size() == 0)
            return;

        Operation currentOperation = operationStack.peek();
        if (currentOperation.priority >= minPriority) {
            double arg1;
            double arg2;
            double result = 0.0;
            currentOperation = operationStack.pop();
            switch (currentOperation.sign) {
                case '+':
                    arg1 = argumentStack.pop();
                    arg2 = argumentStack.pop();
                    result = arg2 + arg1;
                    argumentStack.push(result);
                    break;
                case '-':
                    arg1 = argumentStack.pop();
                    arg2 = argumentStack.pop();
                    result = arg2 - arg1;
                    argumentStack.push(result);
                    break;
                case '*':
                    arg1 = argumentStack.pop();
                    arg2 = argumentStack.pop();
                    result = arg2 * arg1;
                    argumentStack.push(result);
                    break;
                case '/':
                    arg1 = argumentStack.pop();
                    arg2 = argumentStack.pop();
                    result = arg2 / arg1;
                    argumentStack.push(result);
                    break;
            }
            if (Double.isNaN(result)) {
                throw new CalculationException("Неопределенный результат операции");
            }
            else if (Double.isInfinite(result)) {
                throw new CalculationException("Переполнение");
            }

            if (currentOperation.priority >= minPriority) {
                calculateCurrentState(minPriority);
            }

        }
    }

}
