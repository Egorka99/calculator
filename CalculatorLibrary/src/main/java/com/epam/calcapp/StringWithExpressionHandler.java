package com.epam.calcapp;

import java.util.ArrayList;
import java.util.List;

/**
 * This class work with string with expression. It calculates square roots and breaks the string into tokens.
 * @author Egor_Zavgorodnev
 */
public class StringWithExpressionHandler {

    /**
     * Input not processed expression string 
     */
    private String expression;

    /**
     * Defines the form of the root in the expression
     */
    private static String squareRootNotation = "sqrt";

    public StringWithExpressionHandler(String expression) {
        this.expression = expression;
    }

    /**
     * Split string into parts
     * @throws InputDataException
     */
    public List<Part> breakStringIntoParts() throws InputDataException {

        expression = expressionWithoutRoot(expression);

        if (expression.length() > 100 )
            throw new InputDataException("Строка слишком длинная");

        if (expression.length() == 0 )
            throw new InputDataException("Пустая строка");

        List<Part> parts = new ArrayList<>();
        int index = 0;

        boolean needNumber = true; // expect number
        while(index < expression.length()) {

            Part newPart = new Part();
            switch (expression.charAt(index)) {
                //обработка:
                //знаки
                case ' ':
                    index++;
                    break;
                case '+':
                case '-':
                case '*':
                case '/':
                    newPart.value = String.valueOf(expression.charAt(index));
                    newPart.type = PartType.sign;
                    newPart.position = index + 1;
                    parts.add(newPart);
                    index++;
                    if (needNumber) throw new InputDataException("Два оператора подряд недопустимо");
                    needNumber = true;
                    break;
                //числа
                default:
                    if ((expression.charAt(index) >= '0' && expression.charAt(index) <= '9') || (expression.charAt(index) == '.')) {
                        if (!needNumber) throw new InputDataException("Два числа подряд недопустимо");
                        needNumber = false;
                        newPart.value = "";
                        newPart.type = PartType.number;
                        newPart.position = index + 1;

                        while ((expression.charAt(index) >= '0' && expression.charAt(index) <= '9') || (expression.charAt(index) == '.')) {

                            newPart.value += expression.charAt(index);
                            index++;
                            if (index == expression.length()) {
                                break;
                            }
                        }
                        index--;
                        if (!tryParseDouble(newPart.value)) {
                            throw new InputDataException("Неверное число: позиция " + newPart.position);
                        }
                        parts.add(newPart);
                    }
                    else {
                        throw new InputDataException("Недопустимый символ: позиция " + index + 1);
                    }
                    index++;
                    break;
            }
        }

        if (parts.size() < 3) {
            throw new InputDataException("Количество лексем слишком мало. Выражение составлено неверно");
        }

        if (parts.get(parts.size() - 1).type == PartType.sign) {
            throw new InputDataException("Незаконченное выражение");
        }

        return parts;
    }

    /**
     * root calculation
     * @param expression input expression
     * @return expression with numbers instead of roots
     * @throws InputDataException
     */
    private String expressionWithoutRoot(String expression) throws InputDataException {

        StringBuilder expressionBuilder = new StringBuilder(expression);

        while(expressionBuilder.indexOf(squareRootNotation) != -1) {

            StringBuilder numberUnderRootInString = new StringBuilder();

            int index = expressionBuilder.indexOf(squareRootNotation) + squareRootNotation.length();


            while ( index < expressionBuilder.length()) {
                char currentSymbol = expressionBuilder.charAt(index);
                if ( ( currentSymbol >= '0' && currentSymbol <= '9') || (currentSymbol == '.')) {
                    numberUnderRootInString.append(currentSymbol);
                    index++;
                }
                else break;
            }

            if (!tryParseDouble(numberUnderRootInString.toString())) {
                throw new InputDataException("Неверное выражение под корнем");
            }

            double numberUnderRoot = Double.parseDouble(numberUnderRootInString.toString());

            expressionBuilder = expressionBuilder.replace(expressionBuilder.indexOf(squareRootNotation),
                    index,
                    String.valueOf(Math.sqrt(numberUnderRoot)));
        }
        return  expressionBuilder.toString();
    }

    /**
     * if number correctly converted to double return true, else false
     * @param value string to be converted to double
     * @return
     */
    private static boolean tryParseDouble(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

}
