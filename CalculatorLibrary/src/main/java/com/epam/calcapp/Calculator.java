package com.epam.calcapp;

/**
 *  Interface for calculator program
 *
 * @author Egor_Zavgorodnev
 */
public interface Calculator {
    /**
     * Calculation method
     * @param expression given expression
     * @return calculation result
     */
    double calculate(String expression) throws InputDataException, CalculationException;
}
