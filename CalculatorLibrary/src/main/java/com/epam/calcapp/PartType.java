package com.epam.calcapp;

/**
 * Enum for definition type of part
 */
enum PartType // Тип лексемы
{
    number,
    sign,
}
