package com.epam.calcapp;

/**
 * Part - number or sign
 */
class Part
{
    /**
     * Value - for example "245" or "+"
     */
    public String value;
    /**
     * Number or sign
     */
    public PartType type;
    /**
     * Position in expression
     */
    public int position;
}
