package com.epam.calcapp;

/**
 * Operation structure
 */
class Operation {
    /**
     * Operation sign (+,-,*,/)
     */
    public char sign;
    /**
     * Operation priority to correctly evaluate the expression
     * *,/ priority 2, +,- priority 1
     */
    public int priority;
}
