package com.epam.calcapp;

import static org.junit.Assert.assertEquals;

import com.epam.calcapp.suite.CalculationTests;
import com.epam.calcapp.suite.ParserTests;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Unit test for CalculatorLibrary
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(value = {CalculationTests.class, ParserTests.class})
public class LibraryTest
{
}
