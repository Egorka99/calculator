package com.epam.calcapp.suite;

import com.epam.calcapp.*;
import org.junit.Test;

import static org.junit.Assert.assertFalse;

public class ParserTests {

    StringWithExpressionHandler stringWithExpressionHandler;

    @Test
    public void parseTestWithoutSpaces() throws InputDataException {
        stringWithExpressionHandler = new StringWithExpressionHandler("5/5");

        assertFalse(stringWithExpressionHandler.breakStringIntoParts().isEmpty());
    }

    @Test
    public void parseTestWithLotsOfSpaces() throws InputDataException {
        stringWithExpressionHandler = new StringWithExpressionHandler("     5         /  5     ");

        assertFalse(stringWithExpressionHandler.breakStringIntoParts().isEmpty());
    }

    @Test(expected = InputDataException.class)
    public void parseTestAbracadabra() throws InputDataException {
        stringWithExpressionHandler = new StringWithExpressionHandler("@#@$@$(()@$@EFEFEWF");

        stringWithExpressionHandler.breakStringIntoParts();

    }

    @Test(expected = InputDataException.class)
    public void parseTestDualOperator() throws InputDataException {
        stringWithExpressionHandler = new StringWithExpressionHandler("5 ++ 5");

        stringWithExpressionHandler.breakStringIntoParts();

    }

    @Test(expected = InputDataException.class)
    public void parseTestUnfinishedExpression() throws InputDataException {
        stringWithExpressionHandler = new StringWithExpressionHandler("5 +");

        stringWithExpressionHandler.breakStringIntoParts();

    }

    @Test(expected = InputDataException.class)
    public void parseTestRootOfNegativeNumber() throws InputDataException {
        stringWithExpressionHandler = new StringWithExpressionHandler("sqrt-1");

        stringWithExpressionHandler.breakStringIntoParts();

    }

    @Test(expected = InputDataException.class)
    public void parseTestTooLargeNumbers() throws InputDataException {

        stringWithExpressionHandler = new StringWithExpressionHandler("9223372036854775807768888888888888888888888888888888888888888888888888888888888" +
                "8888887687687687687687687687687687687687687687687687687687" +
                "6876876876876876876876876876876876876876876876 - 1");

        stringWithExpressionHandler.breakStringIntoParts();

    }

    @Test(expected = InputDataException.class)
    public void parseTestTooLargeExpression() throws InputDataException{
        stringWithExpressionHandler = new StringWithExpressionHandler("2 + 3 - 45.5 + 90 - 20 - 8 + 20 * sqrt4 + 2 + 3 - 45.5 + 90 - 20 - 8 + 20 * sqrt4 " +
                "+ 2 + 3 - 45.5 + 90 - 20 - 8 + 20 * sqrt4 + 2 + 3 - 45.5 + 90 - 20 - 8 + 20 * sqrt4 - 2 + 3 - 45.5 + 90 - 20 - 8 + 20 * sqrt4");

        stringWithExpressionHandler.breakStringIntoParts();
    }

    @Test(expected = InputDataException.class)
    public void parseTestExpressionIsEmpty() throws InputDataException {

        stringWithExpressionHandler = new StringWithExpressionHandler("");

        stringWithExpressionHandler.breakStringIntoParts();

    }

    @Test(expected = InputDataException.class)
    public void parseTestSpacesInsteadOfExpression() throws InputDataException {

        stringWithExpressionHandler = new StringWithExpressionHandler("      ");

        stringWithExpressionHandler.breakStringIntoParts();

    }

    @Test(expected = InputDataException.class)
    public void parseTestOneNumber() throws InputDataException {

        stringWithExpressionHandler = new StringWithExpressionHandler("5");

        stringWithExpressionHandler.breakStringIntoParts();

    }

    @Test(expected = InputDataException.class)
    public void parseTestOneSign() throws InputDataException {

        stringWithExpressionHandler = new StringWithExpressionHandler("*");

        stringWithExpressionHandler.breakStringIntoParts();

    }
}
