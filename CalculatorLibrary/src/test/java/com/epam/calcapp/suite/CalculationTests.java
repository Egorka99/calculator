package com.epam.calcapp.suite;

import com.epam.calcapp.CalculationException;
import com.epam.calcapp.Calculator;
import com.epam.calcapp.ExpressionEvaluation;
import com.epam.calcapp.InputDataException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CalculationTests {

    Calculator calculator;

    @Before
    public void setUp() {
        calculator = new ExpressionEvaluation();
    }

    @Test
    public void calculateTestWithPriorityOne() throws CalculationException, InputDataException {

        double actual = calculator.calculate("2 + 3 + 4 - 5 + 77");

        assertEquals(2 + 3 + 4 - 5 + 77,actual,1e-9);
    }

    @Test
    public void calculateTestWithPriorityTwo() throws CalculationException, InputDataException {
        double actual = calculator.calculate("2 * 3 * 4 / 5 * 77");

        assertEquals(369.6, actual ,1e-9);
    }

    @Test
    public void calculateTestWithDifferentPriorities() throws InputDataException, CalculationException {

        double actual = calculator.calculate("2 * 3 + 45 * 90 * 20 - 8 / 20");

        assertEquals( 81005.6, actual,1e-9);
    }

    @Test
    public void calculateTestNegativeResult() throws InputDataException, CalculationException {

        double actual = calculator.calculate("5 - 6");

        assertEquals(-1,actual,1e-9);
    }

    @Test
    public void calculateTestOperationWithFractalNumbers() throws InputDataException, CalculationException {

        double actual = calculator.calculate("45.5 - 44.8 * 5.5 + 6.56 / 1.1");

        assertEquals(45.5 - 44.8 * 5.5 + 6.56 / 1.1,actual,1e-2);
    }


    @Test
    public void calculateTestSqrt() throws InputDataException, CalculationException {

        double actual = calculator.calculate("sqrt4 + sqrt4");

        assertEquals(4,actual,1e-9);
    }

    @Test
    public void calculateTestRootOfFractalNumber() throws InputDataException, CalculationException {

        double actual = calculator.calculate("sqrt45.5 + sqrt45.5");

        assertEquals(13.49,actual,1e-2);
    }

    @Test(expected = CalculationException.class)
    public void calculateTestDivideByZero() throws InputDataException, CalculationException {

        calculator.calculate("1 / 0");

    }

    @Test
    public void calculateTestAdditionCommutativity() throws InputDataException, CalculationException {
        assertEquals(calculator.calculate("4 + 5"), calculator.calculate("5 + 4"),1e-9);
    }


    @Test
    public void calculateTestSubstractionAnticommutativity() throws InputDataException, CalculationException {
        assertNotEquals(calculator.calculate("10-20"), calculator.calculate("20-10"));
    }


    @Test
    public void calculationMultiplicationCommutativity() throws InputDataException, CalculationException {
        assertEquals(calculator.calculate("15*9"), calculator.calculate("9*15"),1e-9);
    }

    @Test
    public void calculateTestDivisionAntiCommutativity() throws InputDataException, CalculationException {
        assertNotEquals(calculator.calculate("31/351"), calculator.calculate("351/31"));
    }




}
